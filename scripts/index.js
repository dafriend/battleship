//TO-DO:

//I'd also like to add functionality that allows for players who are sunk to be out of the
//game - what I'm thinking is a button by each person's name. When it's clicked the person's 
//name will be striked through (or something like that) and then the "next turn" function
//will automatically skip those players.

//It'd also be cool to add a button next to each player's name that when clicked only colors
//that player's shots, and makes the other shots have no background color.

//I also need to add the ability for users to add players

//Also I'd like to make the overflow of the ship tracker do a scroll bar


var NROW = 10;
var NCOL = 20;
//var PLAYERS = ["Derek", "Mom", "Dad", "Emily", "TJ", "Nathan", "Bethany", "A", "B", "C", "D", "E", "F", "G"];
//var PLAYERS = ["Derek"];
//var ALPHA = .4;
var ALPHA = .4;

var grid_div_id = "battleship_grid_div";
//var player_select_id = "player_select";
//var player_list_div = "player_list_div";
var player_list_group = "player_list_group";
var next_turn_button_id = "next_turn_button";
var player_radio_name = "player_radio";
var shot_number_name = "shot_number";
var ship_tracker_id = "ship_tracker_div";
var border_button_div = "border_button_div";
var border_buttons = "edit_border_button";
var erase_button = "erase_shot";
var add_border_button = "add_border_button";
//var erase_val = "__erase__";
var erase_val = -1;

var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

//================================================================
//create a dynamic stylesheet
//================================================================
//https://davidwalsh.name/add-rules-stylesheets
var sheet = (function() {
	// Create the <style> tag
	var style = document.createElement("style");

	// Add a media (and/or media query) here if you'd like!
	// style.setAttribute("media", "screen")
	// style.setAttribute("media", "only screen and (max-width : 1024px)")

	// WebKit hack :(
	style.appendChild(document.createTextNode(""));

	// Add the <style> element to the page
	document.head.appendChild(style);

	return style.sheet;
})();
//================================================================
//Set up player dropdown
//================================================================

// var select = document.getElementById(player_select_id);
// for(var i = 0; i < PLAYERS.length; i++){
//     var opt = document.createElement("option");
//     opt.value = i;
//     opt.text = PLAYERS[i];
//     select.add(opt, null);
// }

//================================================================
//Set up color palette for players
//================================================================
// let colorMap = new jPalette.ColorMap(100, [
//     new jPalette.Color(255, 0, 0, 255),
//     new jPalette.Color(0, 255, 0, 255),
//     new jPalette.Color(0, 0, 255, 255),
//   ]);
var colorMap = new ColorMap(1000, [
    new Color(255,   0,   0, ALPHA),
    new Color(255, 128,   0, ALPHA),
    new Color(255, 255,   0, ALPHA),
    new Color(0  , 255,   0, ALPHA),
    new Color(0  , 255,  128, ALPHA),
    new Color(0  , 255,  255, ALPHA),
    new Color(0  ,   0,  255, ALPHA),
    new Color(128,   0,  255, ALPHA),
    new Color(255,   0,  255, ALPHA)
  ]);


function colorToRGBA(color, alpha){
    return "rgba(" + color.r + "," + color.g + "," + color.b + "," + alpha + ")";
}

//var colorMap = jPalette.ColorMap.get('rainbow')(1000);

//-----------------------
//add player colors to stlyesheet
//-----------------------


// Normalized index between 0 and 1
//let color = colorMap.getColor(0.3);

//console.log(color);
// Color {r: 105, g: 149, b: 0, a: 255}
//console.log(color.rgb());


//================================================================
//Set up player table
//================================================================

//creates an HTML player for a single player, to be used in the 
//list of players
function createPlayerElement(playerName, playerID){
    var playerCell = document.createElement("div");
    playerCell.classList.add("list-group-item");
    playerCell.setAttribute("data-playerid", playerID);
    if(playerID == 0){
        playerCell.classList.add("active");
    }
    playerCell.classList.add("player" + playerID);
    playerCell.setAttribute("data-playerid", playerID);

    var arrowIcon = document.createElement("i");
    arrowIcon.className = "fas fa-arrows-alt-v";
    var arrowSpan = document.createElement("span");
    arrowSpan.className = "player-handle float-right";
    arrowSpan.appendChild(arrowIcon);

    var skullIcon = document.createElement("i");
    skullIcon.className = "fas fa-skull-crossbones";
    var skullSpan = document.createElement("span");
    skullSpan.className = "player-skull float-right";
    skullSpan.appendChild(skullIcon);
    $(skullSpan).on("click", function(){
        $(this).closest(".list-group-item").toggleClass("dead");
    });

    
    var playerNameSpan = document.createElement("span");
    playerNameSpan.className = "player-name";
    playerNameSpan.innerHTML = playerName;
    $(playerNameSpan).on("click", function(){
        //$(tableDiv).find(".list-group-item").removeClass("active");
        $(this).closest(".list-group-item").siblings().removeClass("active");
        $(this).closest(".list-group-item").addClass("active");
    });

    playerCell.appendChild(playerNameSpan);
    playerCell.appendChild(arrowSpan);
    playerCell.appendChild(skullSpan);

    return(playerCell);
}

// var playerList = document.createElement("div");
// playerList.classList.add("list-group");

// for(var i = 0; i < PLAYERS.length; i++){
//     var playerElement = createPlayerElement(PLAYERS[i], i);
//     playerList.appendChild(playerElement);
// }
// //var table_div = document.getElementById(player_table_id);
// //table_div.appendChild(player_table);
// var tableDiv = document.getElementById(player_table_id);
// tableDiv.appendChild(playerList);

// $(tableDiv).find(".player-skull").on("click", function(){
//     $(this).closest(".list-group-item").toggleClass("dead");
// });

// $(tableDiv).find(".list-group-item .player-name").on("click", function(){
//     $(tableDiv).find(".list-group-item").removeClass("active");
//     $(this).closest(".list-group-item").addClass("active");
// })


//================================================================
//Create addShot function for adding/removing a shot to a cell
//================================================================
//function for adding/removing a shot from a cell
function addShot(el){
    var selectedPlayerId = $('#player_list_div div.active').attr("data-playerid"); // get the player that's selected in the player list
    var turnNumber = $('input[name="' + shot_number_name + '"]').val(); // get the shot number from the input element
    $(el).removeClass(function (index, className) { //remove any class with the pattern 'player*'
        return (className.match(/player\d/g) || []).join(' ');
    });
    var erase = $("#" + erase_button); //get the button for erasing values
    if(erase.hasClass("active")){ //if the erase button is active, remove the text from the cell
        $(el).find(".cell_text").html(""); //get rid of the text
        $(el).removeClass("has_text"); //take off the 'has_text' class
    } else { //otherwise add the shot number and the current player's class to this element
        el.classList.add("player" + selectedPlayerId); //add this player's class to the element
        $(el).find(".cell_text").html(turnNumber); //add the shot number
        $(el).addClass("has_text"); //add the 'has_text' class
    }
}
//================================================================
//Set up ship hit tracker functions
//================================================================

//creates and returns a <table> element to be used as a ship
//PARAMETERS:
//  nx -> integer; the number of cells in the x direction
//  ny -> integer; the number of cells in the y direction
//  borderCells -> 2-D array; essentially an array of 2-element (integer) arrays
//          that represent cell locations (assumes (0,0) is the lower-left corner). These
//          cells will be assigned the 'bordered_cell' class while all other cells will
//          have the class 'unbordered_cell'
//  callback -> function to be assigned to each cell on the 'click' event.
//RETURNS:
//  an HTML table element
function createShipTable(nx, ny, borderCells, callback){
    var table = document.createElement("table");
    table.className = "ship_table";
    for(var y = 0; y < ny; y++){
        var row = document.createElement("tr");
        for(var x = 0; x < nx; x++){
            var cell = document.createElement("td")
            //cell.setAttribute("data-x", x);
            //cell.setAttribute("data-y", y);
            
            cell.className = "unbordered_cell";
            for(var i = 0; i < borderCells.length; i++){
                if(borderCells[i][0] == x & borderCells[i][1] == y){
                    cell.className = "bordered_cell";
                    cellText = document.createElement("span");
                    cellText.classList.add("cell_text");
                    cell.appendChild(cellText);
                    cell.addEventListener('click',(function(el){
                        return function(){
                            callback(el);
                        }
                    })(cell),false);
                }
                //var cell = $(table).find("td[data-x='" + borderCells[i][0] + "'][data-y='" + borderCells[i][1] + "']");
                
                //console.log(cell);
                //$(table).find("td[data-x='" + borderCells[i][0] + "'][data-y='" + borderCells[i][1] + "']").className = "bordered_cell";
            }
            row.appendChild(cell);
        }
        table.appendChild(row);
    }

    for(var i = 0; i < borderCells.length; i++){
        var cell = $(table).find("td[data-x='" + borderCells[i][0] + "'][data-y='" + borderCells[i][1] + "']");
        cell.className = "bordered_cell";
        //console.log(cell);
        //$(table).find("td[data-x='" + borderCells[i][0] + "'][data-y='" + borderCells[i][1] + "']").className = "bordered_cell";
    }
    return(table);
}


//creates a table representing each ship
//PARAMETERS:
//  type -> character; one of "airplane", "battleship", "cruiser", "destroyer", or "submarine"
//  callback -> callback function that will be given to the 'callback' parameter of 'createShipTable()'
//RETURNS:
//  an HTML table element
function createShip(type, callback){
    var nx;
    var ny;
    var borderCells;
    switch(type){
        case "airplane":
            borderCells = [[0,0], [0,1], [0,2], [1,1], [2,1]];
            nx = 3;
            ny = 3;
            break;
        case "battleship":
            borderCells = [[0,1], [1,1], [2,1], [3,1], [4,1]];
            nx = 5;
            ny = 3;
            break;
        case "cruiser":
            borderCells = [[0,1], [1,1], [2,1], [3,1]];
            nx = 4;
            ny = 3;
            break;
        case "destroyer":
            borderCells = [[0,1], [1,1], [2,1]];
            nx = 3;
            ny = 3;
            break;
        case "submarine":
            borderCells = [[0,1], [1,1]];
            nx = 2;
            ny = 3;
    }
    //var borderCells = [[0,0], [0,1], [0,2], [1,1], [2,1]];
    var ship = createShipTable(nx,ny,borderCells, callback);
    ship.classList.add(type);
    return(ship);
}

//================================================================
//Set up ship hit tracker for each player
//================================================================

//creates an HTML element for a ship tracker (i.e. a div that shows the five types of ships)
//PARAMETERS:
//  playerName -> string; the player name to use for the tracker
//  playerID -> integer; ID of the player. This will be used to generate the class
//      of the resulting <div> and will also be assigned to the 'data-playerid' attribute
//RETURNS:
//An HTML <div> element 
function createShipTracker(playerName, playerID){
    var tracker = document.createElement("div");
    tracker.className = "row ship_tracker";
    tracker.setAttribute("data-playerid", playerID);
    // var trackerHeader = document.createElement("div");
    // trackerHeader.className = "card-header player" + i;
    // trackerHeader.innerHTML = PLAYERS[i];
    
    // var trackerBody = document.createElement("div");
    // trackerBody.className = "card-body row";


    var trackerNameDiv = document.createElement("div");
    trackerNameDiv.className = "tracker_name_div col-xs-2 text-center align-middle player" + playerID;
    
    var trackerNameSpan = document.createElement("span");
    trackerNameSpan.innerHTML = playerName;
    trackerNameDiv.appendChild(trackerNameSpan);
    tracker.appendChild(trackerNameDiv);

    var trackerShipDiv = document.createElement("div");
    trackerShipDiv.className = "col-xs-10 tracker_ship_div";

    trackerShipDiv.appendChild(createShip("airplane", addShot));
    trackerShipDiv.appendChild(createShip("battleship", addShot));
    trackerShipDiv.appendChild(createShip("cruiser", addShot));
    trackerShipDiv.appendChild(createShip("destroyer", addShot));
    trackerShipDiv.appendChild(createShip("submarine", addShot));

    tracker.appendChild(trackerShipDiv);
    return tracker;
}


//create a ship tracker for each player
// var shipDiv = document.getElementById(ship_tracker_id);
// for(var i = 0; i < PLAYERS.length; i++){
//     var tracker = createShipTracker(PLAYERS[i], i);
//     shipDiv.appendChild(tracker);
// }

// var playerList = document.createElement("div");
// playerList.classList.add("list-group");

// for(var i = 0; i < PLAYERS.length; i++){
//     var playerElement = createPlayerElement(PLAYERS[i], i);
//     playerList.appendChild(playerElement);
// }
// //var table_div = document.getElementById(player_table_id);
// //table_div.appendChild(player_table);
// var tableDiv = document.getElementById(player_table_id);
// tableDiv.appendChild(playerList);



// var colorStep = 1/PLAYERS.length;
// for(var i = 0; i < PLAYERS.length; i++){
//     // playerMap.set(i, colorToRGBA(colorMap.getColor(i*colorStep), ALPHA));
//     var color = colorToRGBA(colorMap.getColor(i*colorStep), ALPHA);
//     var style = document.createElement("style");
//     style.id = "player" + i + "-style";
//     // style.innerHTML = ".player" + i + " { background-color: " + playerMap.get(i) + " !important}";
//     style.innerHTML = ".player" + i + " { background-color: " + color + " !important}";
//     //style.sheet.insertRule(".player" + i + " { background-color: " + playerMap.get(i) + " !important}");
//     document.head.appendChild(style);

// }

function addPlayer(playerName, id){
    //add entry to the player list
    var playerList = document.getElementById(player_list_group);
    var playerElement = createPlayerElement(playerName, id);
    playerList.appendChild(playerElement);

    //add a ship tracker for this player
    var shipDiv = document.getElementById(ship_tracker_id);
    var tracker = createShipTracker(playerName, id);
    shipDiv.appendChild(tracker);

    //add a CSS style for this player's color
    var playerElements = $("#player_list_group .list-group-item");
    var colorStep = 1/playerElements.length;
    for(var i = 0; i < playerElements.length; i++){
        var color = colorToRGBA(colorMap.getColor(i*colorStep), ALPHA);
        var playerID = $(playerElements[i]).attr("data-playerid");
        var style = $("#player" + playerID + "_style");
        if(style.length == 0){
            style = $("<style></style>");
            style.attr("id", "player" + playerID + "_style");
        } else {
            style.html("");
        }
        style.html(".player" + playerID +  " { background-color: " + color + " !important}");
        $("head").append(style);
    }
}


$("#new_player_button").on("click", function(){
    var new_player_div = $("#new_player_div");
    new_player_div.removeClass("div-hide-input");
    new_player_div.addClass("div-show-input");
});

$("#reject_player_button").on("click", function(){
    var new_player_div = $("#new_player_div");
    new_player_div.removeClass("div-show-input");
    new_player_div.addClass("div-hide-input");
});

var playerIdCounter = 0;
$("#accept_player_button").on("click", function(){
    //var new_player_div = $("#new_player_div");
    var newPlayerInput = $("#new_player_input");
    var playerName = newPlayerInput.val();
    if(playerName.length > 0){
        addPlayer(playerName,playerIdCounter);
        newPlayerInput.val("");
        playerIdCounter++;
    }
})

//trigger clicking 'accept_player_button' when enter is pressed in textbox; https://stackoverflow.com/questions/155188/trigger-a-button-click-with-javascript-on-the-enter-key-in-a-text-box
$("#new_player_input").keyup(function(event) {
    if (event.keyCode === 13) {
        $("#accept_player_button").click();
    }
});

//var idTracker = 0;
// for(var i = 0; i < PLAYERS.length; i++){
//     addPlayer(PLAYERS[i],i);
// }
//================================================================
//Add "sortable" to player list and link it up with the ship trackers
//================================================================

var playerList = document.getElementById(player_list_group);
$(playerList).sortable({
    handle: ".player-handle",
    update: function(event, ui){ //make it so that the ship trackers also sort when the player list is sorted
        var playerDivs = $(playerList).find(".list-group-item") //get the sorted elements
        var idsOrdered = $.map(playerDivs, function(thing){ //get the IDs of the newly ordered players
            return $(thing).attr("data-playerid");
        })

        var tracker_div = $("#"+ship_tracker_id); //get the div with the trackers
        var tracker_clone = tracker_div.clone() //clone it so we still have the elements after we empty 'tracker_div'
        tracker_div.empty(); //delete all children in the tracker div
        for(var i = 0; i < idsOrdered.length; i++){ //loop over the ordered IDs and add the trackers back into the div in the correct order
            var tracker_i = tracker_clone.find('.ship_tracker[data-playerid="' + idsOrdered[i] +'"]');
            tracker_div.append(tracker_i);
        }
        // cell.addEventListener('click',(function(el){
        //     return function(){
        //         callback(el);
        //     }
        // })(cell),false);
        $(".ship_table .bordered_cell").on("click", function(){
            addShot(this);
        });
    }
});


//================================================================
//Set up erase button
//================================================================
function toggleErase(){
    // $("#" + erase_button).toggleClass("active");
    $("#erase_shot").toggleClass("active");
}
$("#" + erase_button).on("click", toggleErase);
//===========================================

//================================================================
//Set up border drawing button
//================================================================
var borderButton = document.getElementById(add_border_button);

function toggleBorder(){
    $("#" + add_border_button).toggleClass("active");
}
$(borderButton).on("click", toggleBorder);
//================================================================
//Set up clickable grid
//================================================================
//Code modified from:
//https://stackoverflow.com/questions/9140101/creating-a-clickable-grid-in-a-web-browser
function clickableGrid( rows, cols, callback ){
    var i=0;
    var grid = document.createElement('table');
    grid.className = 'battleship_grid';
    for (var r=0;r<rows;++r){
        var tr = grid.appendChild(document.createElement('tr'));
        for (var c=0;c<cols;++c){
            var cell = tr.appendChild(document.createElement('td'));
            if(c == 0 && r >= 1){
                //cell.innerHTML =  r;
                
                cell.innerHTML =  alphabet.charAt((r % alphabet.length)-1);
            }
            if(r == 0 && c >= 1){
                cell.innerHTML = c;
            }
            if(r != 0 && c != 0){
                
                cellText = document.createElement("span");
                cellText.classList.add("cell_text");
                cell.appendChild(cellText);
                cell.addEventListener('click',(function(el){
                    return function(){
                        callback(el);
                    }
                })(cell),false);
            } else {
                cell.style.backgroundColor = "#EEEEEE";
            }
        }
    }
    return grid;
}



function toggleShip(el){
    // if(el.classList.contains("bordered_cell")){
    //     el.classList.remove("bordered_cell");
    // } else {
    //     el.classList.add("bordered_cell");
    // }
    //el.classList.toggle("bordered_cell");
    $(el).toggleClass("contains_ship");

    //console.log('hi there');
    return false;
}

function cellListener(el){
    
    if($("#" + add_border_button).hasClass("active")){
        toggleShip(el);
    } else {
        addShot(el);
    }
}
//var grid = clickableGrid(NROW+1,NCOL+1,addShotListener);
var grid = clickableGrid(NROW+1,NCOL+1,cellListener);

var gridDiv = document.getElementById(grid_div_id);
gridDiv.appendChild(grid);
//document.body.appendChild(grid);

function incrementTurn(){
    var alivePlayers = $("#player_list_div .list-group-item:not(.dead)"); //get players who are alive
    if(alivePlayers.length > 0){ //if there are no players left alive, do nothing (without this check we'd get an infinite loop when all the players are dead)
        var selectedPlayer = $('#player_list_div div.active'); //get the currently selected player
        var playerIndex = selectedPlayer.index(); //get its position in the list
        var turnInput =$('input[name="' + shot_number_name + '"]'); //get the turn number from the "turn number" input
        var nPlayers = $("#player_list_div .list-group .list-group-item").length; //get the total number of players
        var nextPlayerIndex = ((playerIndex + 1) % nPlayers) + 1; //get the index of the next player
        var nextPlayer = $("#player_list_div .list-group-item:nth-child(" + nextPlayerIndex + ")"); // get the next player

        
        selectedPlayer.removeClass("active"); //make the current player "inactive"
        $(nextPlayer).addClass("active"); // make the next player "active"
        if($(nextPlayer).hasClass("dead")){ // we want to skip any players who are dead
            //nextPlayer.find("name-span").trigger("click");
            incrementTurn(); //if the next player is dead, don't add 1 to the turn number and then run 'incrementTurn()' again
        } else {
            turnInput.val(parseInt(turnInput.val()) + 1); //add one to the turn number
        }
    }
    
}

document.getElementById(next_turn_button_id).addEventListener('click',incrementTurn);

// function toggleBorderButtons(){

// }
// borderButtons = document.getElementsByClassName(border_buttons);
// for(i = 0; i < borderButtons.length; i++){
//     borderButtons[i].addEventListener
// }



